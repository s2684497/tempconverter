package converter;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
public class Converter extends HttpServlet{
    private Calculator calculator;
    public void init() throws ServletException{
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Conversion";

        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("temp") + "\n" +
                "  <P>Fahrenheit: " +
                calculator.convertToFahrenheit(Double.parseDouble(request.getParameter("temp")))+
                "</BODY></HTML>");
    }
}
