package converter;

public class Calculator {

    public double convertToFahrenheit(double celsius){
        return (celsius * 9 / 5) + 32;
    }
}
